import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import 'firebase-functions';
export * from './deploy3';

admin.initializeApp({
    credential: admin.credential.applicationDefault()
});

export const db = admin.firestore();
export const auth = admin.auth;
// export { admin };

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld2 = functions.https.onRequest((request, response) => {
//     functions.logger.info('Hello logs!', { structuredData: true });
//     response.send('Hello from Firebase!');
// });

import './mockdata/userAuthmock';

// export const createStaff = functions
//     .region('asia-southeast2')
//     .https.onRequest(async (req, res) => {
//         db.collection('staffs')
//             .doc(req.body.authId)
//             .set({
//                 firstname: req.body.firstname,
//                 lastname: req.body.lastname,
//                 nickname: req.body.nickname,
//                 role: req.body.role,
//                 salary: req.body.salary,
//                 paymentType: req.body.paymentType,
//                 telephone: req.body.telephone,
//                 share_percent: req.body.share_percent,
//                 department: req.body.department,
//                 line: req.body.line,
//                 facebook: req.body.facebook,
//                 email: req.body.email,
//                 national_id: req.body.national_id,
//                 birthday: req.body.birthday,
//                 image: req.body.image,
//                 gender: req.body.gender,
//                 address: req.body.address
//             })
//             .then((d) => {
//                 console.log('Document successfully written!', d);

//                 res.status(200).send('staff was created.');
//             })
//             .catch((error) => {
//                 console.error('Error writing document: ', error);
//                 res.status(500).send('Caugth some error.');
//             });
//     });

export const createStaff = functions
    .region('asia-southeast2')
    .https.onCall(async (data, context) => {
        try {
            console.log('data from functions', data);

            return await db
                .collection('staffs')
                .doc(data.uid)
                .set({
                    // firstname: data.firstname,
                    // lastname: data.lastname,
                    // nickname: data.nickname,
                    // role: data.role,
                    // salary: data.salary,
                    // paymentType: 'monthlty',
                    // telephone: data.telephone,
                    // share_percent: data.share_percent,
                    // department: data.department,
                    // line: data.line,
                    // facebook: data.facebook,
                    // email: data.email,
                    // national_id: data.national_id,
                    // birthday: data.birthday,
                    // image: data.image,
                    // gender: data.gender,
                    // address: data.address
                    ...data
                })
                .then((d) => {
                    console.log('Document successfully written!', d);

                    return { message: 'success' };
                })
                .catch((error) => {
                    console.error('Error writing document: ', error);

                    throw error;
                });
        } catch (e) {
            console.error('Error catch: ', e);

            return { message: 'fail' };
        }
    });

// const createMockAuth = async () => {
//     let uid = '';
//     await admin
//         .auth()
//         .createUser({
//             email: 'aaa@aaa.com',
//             emailVerified: false,
//             phoneNumber: '+66816602200',
//             password: '123456',
//             displayName: 'John Doe',
//             photoURL: 'http://www.example.com/12345678/photo.png',
//             disabled: false
//         })
//         .then((userRecord) => {
//             uid = userRecord.uid;
//             // See the UserRecord reference doc for the contents of userRecord.
//             console.log('Successfully created new user:', userRecord);
//         })
//         .catch((error) => {
//             console.log('Error creating new user:', error);
//         });

//     await db
//         .collection('staffs')
//         .doc(uid)
//         .set({
//             firstname: 'aaaa',
//             lastname: 'bbbb',
//             nickname: 'xxxx',
//             role: 'admin',
//             salary: 25000.01,
//             paymentType: 'monthlty',
//             telephone: '0816602200',
//             share_percent: 50,
//             department: 'administrator',
//             line: null,
//             facebook: null,
//             email: 'aaa@aaa.com',
//             national_id: null,
//             birthday: new Date(),
//             image: null,
//             gender: 'mail',
//             address: null
//         })
//         .then((d) => {
//             console.log('Document successfully written!', d);

//             return { message: 'success' };
//         })
//         .catch((error) => {
//             console.error('Error writing document: ', error);

//             throw error;
//         });
// };

// if (functions.config()?.env.config === 'emulator') {
//     createMockAuth();
// }
