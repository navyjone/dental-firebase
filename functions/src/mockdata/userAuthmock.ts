import * as functions from 'firebase-functions';
import { db, auth } from '..';

const createMockAuth = async () => {
    let uid = '';
    await auth()
        .createUser({
            email: 'aaa@aaa.com',
            emailVerified: false,
            phoneNumber: '+66816602200',
            password: '123456',
            displayName: 'john wick',
            photoURL: 'http://www.example.com/12345678/photo.png',
            disabled: false
        })
        .then((userRecord) => {
            uid = userRecord.uid;
            // See the UserRecord reference doc for the contents of userRecord.
            console.log('Successfully created new user:', userRecord);
        })
        .catch((error) => {
            console.log('Error creating new user:', error);
        });

    await db
        .collection('staffs')
        .doc(uid)
        .set({
            firstname: 'aaaa',
            lastname: 'bbbb',
            nickname: 'xxxx',
            role: 'admin',
            salary: 25000.01,
            paymentType: 'monthlty',
            telephone: '0816602200',
            share_percent: 50,
            department: 'administrator',
            line: null,
            facebook: null,
            email: 'aaa@aaa.com',
            national_id: null,
            birthday: new Date(),
            image: null,
            gender: 'mail',
            address: null
        })
        .then((d) => {
            console.log('Document successfully written!', d);

            return { message: 'success' };
        })
        .catch((error) => {
            console.error('Error writing document: ', error);

            throw error;
        });
};

if (functions.config()?.env.config === 'emulator') {
    createMockAuth();
}
